# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is an docker repository of texlive in Alpine Linux OS
Learn more from the following links
* [Learn Alpine](https://alpinelinux.org/)
* [Latex](https://www.latex-project.org/)
* [Texlive](https://www.tug.org/texlive/)

### How do I get set up? ###

* Docker

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact