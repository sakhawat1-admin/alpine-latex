FROM alpine:latest

RUN apk update && apk upgrade

RUN apk add wget \
	git \
	openssl \
	texlive-full \
	make

RUN rm -f /var/cache/apk/*

CMD ["/bin/sh"]